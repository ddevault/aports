# Contributor: Will Sinatra <wpsinatra@gmail.com>
# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=doctl
pkgver=1.66.0
pkgrel=0
pkgdesc="Official command line interface for the DigitalOcean API"
url="https://github.com/digitalocean/doctl"
license="Apache-2.0"
arch="all"
makedepends="go"
subpackages="
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="https://github.com/digitalocean/doctl/archive/v$pkgver/doctl-$pkgver.tar.gz"

build() {
	maj_min=${pkgver%*.*}
	major=${maj_min%.*}
	minor=${maj_min#*.}
	patch=${pkgver#*.*.*}

	_buildmode="-buildmode=pie"
	case $CARCH in
		mips*) _buildmode="" ;;
	esac

	go build \
		-trimpath \
		$_buildmode \
		-mod=readonly \
		-ldflags "-extldflags \"$LDFLAGS\" \
			-X github.com/digitalocean/doctl.Major=$major \
			-X github.com/digitalocean/doctl.Minor=$minor \
			-X github.com/digitalocean/doctl.Patch=$patch \
			-X github.com/digitalocean/doctl.Label=alpine-$pkgrel" \
		./cmd/...
}

check() {
	go test -v -mod=readonly ./integration
}

package() {
	install -Dm 755 doctl -t "$pkgdir/usr/bin"

	# setup completions
	mkdir -p "$pkgdir"/usr/share/bash-completion/completions \
		"$pkgdir"/usr/share/zsh/site-functions \
		"$pkgdir"/usr/share/fish/completions

	"$pkgdir"/usr/bin/doctl completion bash > "$pkgdir"/usr/share/bash-completion/completions/doctl
	"$pkgdir"/usr/bin/doctl completion zsh > "$pkgdir"/usr/share/zsh/site-functions/_doctl
	"$pkgdir"/usr/bin/doctl completion fish > "$pkgdir"/usr/share/fish/completions/doctl.fish
}

sha512sums="
6107faac93f0e1b45a55a571d0448794aedbc472645d39fbe72fe5d039b9f594932461a943e391d52fe259debd79a61b22c9b3822df7f8a524a39de016244973  doctl-1.66.0.tar.gz
"
